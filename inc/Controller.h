// **************************************************************************
//! \file   Controller.h
//! \brief  ���������� ������ ��� ���������� �����
// **************************************************************************

#ifndef __CONTROLLER_H
#define __CONTROLLER_H

#include "ifaces.h"
#include "ListOfPlayers.h"

// ==========================================================================
namespace Game
{
class CMainDlg;
//!   ����� ��������� ������� ���������
class Controller : public iface::iDrawable
{
  public:
     ~Controller();

     //! ������������
     virtual void Redraw() const;

     //! ������� �����������
     virtual void Draw(CPaintDC& dc);

      //! �������� ��������� ������������� ��� ������������� �������
      //! ������� ������ ������� � ���������
      void initialize (std::list<std::string>& names, int nStr);

      //! ������������ ���� ������ �����
      void makeStep(int hole);

      //! ���������� true, ���� ���� �������
      bool isActive() const {return active;};

      //! �������� ��������� ����
      void setActive(bool newState) {active = newState;};

      //! �������������� ��������� ��������� ����
      //! ������ ������� �������� ������ � ���������� ���������
      void reset();

      //! ��������� ������ ������� � ��������� � ���� strName
      void Save(std::string strName) const;

      //! ��������������� ������ ������� � ��������� �� ����� strName
      //! \return true, ���� ����� ��������� ����������
      bool Open(std::string strName);

      //! ������������ ������ ��� �����, � ������� ������ ������ �����
      void deActivateButtons();

  private:
      ListOfPlayers        *playersList;     //!< ������ �������
      tdata::Data          *netStruc;        //!< ���������� ���������� ����
      CMainDlg             *mainDlg;         //!< ������� ���� ���������
      bool                 active;           //!< ���� true, �� ���� �������

      int nStructNum; //!< ����� ��������� ���������
};

} //end of namespace Game
// ==========================================================================
#endif