// **************************************************************************
//! \file   Player.h
//! \brief  ����� ����������� ������
// **************************************************************************

#ifndef __PLAYER_H
#define __PLAYER_H

#include <string>

// ==========================================================================
namespace Game
{
class Player
{
   public:
      //! ������ ��� ������
      void setName (std::string newName) { name = newName; }

      //! �������� ��� ������
      std::string getName () const { return name; }

      //! ���������� true, ���� �����-�����������.
      virtual bool ifMain () const = 0;

   private:
      std::string name; //!< ��� ������
};

}//end of namespace Game
// ==========================================================================
#endif