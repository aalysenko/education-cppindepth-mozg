// **************************************************************************
//! \file   SlavePlayer.h
//! \brief  ����� ����������� �������� ������
// **************************************************************************

#ifndef __SLAVEPLAYER_H
#define __SLAVEPLAYER_H

#include <string>
#include "Player.h"

// ==========================================================================
namespace Game
{
class SlavePlayer : public Player
{
   public:
      bool ifMain () const { return false; }

};

}//end of namespace Game
// ==========================================================================

#endif