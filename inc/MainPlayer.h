// **************************************************************************
//! \file   MainPlayer.h
//! \brief  ����� ����������� ������ ������������
// **************************************************************************

#ifndef __MAINPLAYER_H
#define __MAINPLAYER_H

#include <string>
#include "Player.h"

// ==========================================================================
namespace Game
{
class MainPlayer : public Player
{
   public:
      bool ifMain () const { return true; }
};

}//end of namespace Game
// ==========================================================================
#endif